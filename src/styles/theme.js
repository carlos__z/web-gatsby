export const darkTheme = {
  body: '#121212',
  text: '#FAFAFA',
};

export const lightTheme = {
  body: '#E2E2E2',
  text: '#363537',
};
