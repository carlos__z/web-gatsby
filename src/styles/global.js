import React from 'react'
import { Global, css } from '@emotion/core'
import { useTheme } from 'emotion-theming'

export default function GlobalStyles() {
  const theme = useTheme();
  return (
    <>
      <Global
        styles={css`
          @font-face {
            font-family: 'JetBrains Mono';
            src: url('https://cdn.jsdelivr.net/gh/JetBrains/JetBrainsMono/web/woff2/JetBrainsMono-Regular.woff2') format('woff2'),
              url('https://cdn.jsdelivr.net/gh/JetBrains/JetBrainsMono/web/woff/JetBrainsMono-Regular.woff') format('woff');
            font-weight: 400;
            font-style: normal;
            font-display: swap;
          }
          html {
            font-size: 62.5%;
            box-sizing: border-box;
          }
          *, *:before, *:after {
            box-sizing: inherit;
          }
          body {
            font-family: 'JetBrains Mono', sans-serif;
            background-color: ${theme.body};
            color: ${theme.text};
            /* transition: all 0.25s linear; */
            transition-property: background-color, color;
            font-size: 16px;
            font-size: 1.6rem;
            line-height: 1.6;
            margin: 0;
          }
          h1, h2, h3 {
            font-weight: 400;
          }
          h1 {
            font-size: 6.4rem;
            line-height: 6.4rem;
          }
          h2 {
            font-size: 4.2rem;
            line-height: 2rem;
          }
          h3 {
            font-size: 3.2rem;
            line-height: 4rem;
          }
          ul {
            list-style: none;
            margin: 0;
            padding: 0;
          }
          a {
            color:  ${theme.text};
            text-decoration: none;
          }
          main {
            max-width: 1200px;
            margin: auto;
            padding: 3.2rem 1.6rem 6.4rem;
          }
          .active {
            fill: #DDA448!important;
          } 
          /* svg {
            fill: #fafafa;
          }*/
          
          @media (max-width: 768px) {
            h1 {
              font-size: 4.2rem;
              line-height: 3.2rem;
            }
            h2 {
              font-size: 3rem;
            }
            h3 {
              font-size: 2.4rem;
            }
          }             
        `}
      />
    </>
  )
}
