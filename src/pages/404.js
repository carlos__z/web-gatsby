import React from "react"
import Layout from "../components/layout"

const NotFoundPage = () => (
  <Layout>
    <main>
      <h1>404</h1>
      <p>Nada que ver por acá.</p>
    </main>
  </Layout>
)

export default NotFoundPage
