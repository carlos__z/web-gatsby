import React from 'react'
import Layout from '../components/layout'
import Contact from '../components/contact'

const IndexPage = () => (
  <Layout>
    <Contact />
  </Layout>
)

export default IndexPage
