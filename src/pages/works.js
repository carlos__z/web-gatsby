import React from 'react'
import Layout from '../components/layout'
import WorkSlider from '../components/workSlider'

const IndexPage = () => (
  <Layout>
    <main>
      <h2>Trabajos</h2>
      <WorkSlider />
    </main>
  </Layout>
)

export default IndexPage
