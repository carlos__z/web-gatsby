import React from 'react'
import Layout from '../components/layout'
import TechStack from '../components/techStack'
import Exp from '../components/exp'
import styled from '@emotion/styled'
import { css } from '@emotion/core'

const ColumnContainer = styled.div`
  display: flex;
  flex-direction: row;
  @media (max-width: 768px) {
    flex-direction: column;
  }
`;

const Column = styled.div`
  padding: 0 3rem;
  width: 50%;
  @media (max-width: 768px) {
    width: 100%;
    padding: 1.5rem 5rem 1.5rem 0;
  }
  li {
    padding: 1rem 2.5rem;

    @media (max-width: 768px) {
      font-size: 1.2rem;
      padding: 1rem;
    }

    svg {
      font-size: 2rem;
      height: 2rem;
      vertical-align: middle;
      fill: #fff;

      @media (min-width: 768px) {
        margin: 0 2rem;
      }
      @media (max-width: 768px) {
        margin-right: 1rem;
      }
    }
  }
  .wrapper {
    margin: 0 1rem;
    padding: 1.5rem 0rem;
    border-radius: 0.5rem;
    background-color: rgba(72, 65, 70, 0.1);
    /* background-color: rgb(40, 44, 49); */
    /* box-shadow: 3px 3px 7px 1px rgba(0,0,0,0.15); */
    box-shadow: rgba(0, 0, 0, 0.15) 0px 1px 10px 0px, 
      rgba(0, 0, 0, 0.15) 0px 6px 12px 0px, 
      rgba(0, 0, 0, 0.15) 0px 6px 15px -2px;
    @media (min-width: 768px) {
      height: 40vh;
    }
  }
  .center-flex {
    display: flex;
    align-items: center;
  }
`;

const IndexPage = () => (
  <Layout>
    <main>
      <div
        css={css`
          display: flex;
          flex-direction: column;
        `}
      >
        <div>
          <h2>Acerca de mi</h2>
        </div>
        <ColumnContainer>
          <Column>
            <TechStack />
          </Column>
          <Column>
            <Exp />
          </Column>
        </ColumnContainer>
      </div>
      {/* TODO Hobbys */}
    </main>
  </Layout>
)

export default IndexPage
