import React from 'react'
import { FaSatelliteDish, FaPiggyBank, FaChartLine, FaShoppingCart } from 'react-icons/fa'
import { Rocket } from '@emotion-icons/ionicons-solid'
import FadeIn from 'react-fade-in'

export default function Exp() {
  return (
    <>
      <h3>Áreas</h3>
      <div className="wrapper center-flex">
        <ul>
          <FadeIn delay={200} >
            <li><FaSatelliteDish />Telecomunicaciones</li>
            <li><FaPiggyBank />Banca</li>
            <li><FaChartLine />Administrativo / Contable</li>
            <li><Rocket />CoFundador de una Startup</li>
            <li><FaShoppingCart />E-commerce</li>
          </FadeIn>
        </ul>
      </div>
    </>
  )
}
