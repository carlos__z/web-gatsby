import React, { useState } from 'react'
import { Html5, Css3, Javascript, Stylus, Tailwindcss, ReactLogo, 
  Gatsby, Svelte, Php, Mysql, Wordpress, Woo, NodeDotJs, Mongodb, 
  Firebase, Amazonaws, Googlecloud } from '@emotion-icons/simple-icons'
import { Tabs, Tab, Pager, TabPanel, ThemeProvider, defaultTheme } from 'sancho'
import styled from '@emotion/styled'

const StyledTabs = styled(Tabs)`
  .css-10yq0xk-Tabs {
    background: #DDA448;
  }
  margin-bottom: 2rem;
  border-bottom: 0.1rem solid #121212;
`;
const StyledTab = styled(Tab)`
  color: #fafafa;
  line-height: 3.6rem;
  span {
    font-weight: 400;
  }
`;
const ItemList = styled.ul`
  display: flex;
  flex-wrap: wrap;

  li {
    width: 50%;
  }
`;
const theme = {
  ...defaultTheme,
  fonts: {
    ...defaultTheme.fonts,
    sans: "JetBrains Mono, sans-serif",
    base: "JetBrains Mono, sans-serif"
  },
  fontSizes: {
    ...defaultTheme.fontSizes,
    0: "1.8rem",
  },
};

export default function TechStack() {
  const [index, setIndex] = useState(0);
  return (
    <>
      <h3>Tecnologías</h3>   
      <div className="wrapper">
        <ThemeProvider theme={theme}>
          <StyledTabs
            variant="evenly-spaced"
            value={index}
            onChange={i => setIndex(i)}
          >
            <StyledTab id="front">Front-end</StyledTab>
            <StyledTab id="back">Back-end</StyledTab>
            <StyledTab id="ops">Otros</StyledTab>
          </StyledTabs>
        </ThemeProvider>
        <Pager value={index} onRequestChange={i => setIndex(i)}>
          <TabPanel id="front" className="Tab-panel">
            <ItemList>
              <li><Html5 title="HTML 5"/>HTML</li>
              <li><Css3 />CSS</li>
              <li><Stylus />Stylus</li>
              <li><Tailwindcss />Tailwind</li>
              <li><Javascript />JavaScript</li>
              <li><ReactLogo />React</li>
              <li><Gatsby />Gatsby</li>
              <li><Svelte />Svelte</li>
            </ItemList>
          </TabPanel>
          <TabPanel id="back" className="Tab-panel">
            <ItemList>
              <li><Php />PHP</li>
              <li><Mysql />MySQL</li>
              <li><Wordpress />WordPress</li>
              <li><Woo />WooCommerce</li>
              <li><NodeDotJs />NodeJs</li>
              <li><Mongodb />MongoDB</li>
              <li><Firebase />Firebase</li>
            </ItemList>
          </TabPanel>
          <TabPanel id="ops" className="Tab-panel">
            <ul>
              <li><Amazonaws />Amazon Web Services</li>
              <li><Googlecloud />Google Cloud Platform</li>
            </ul>
          </TabPanel>
        </Pager>
      </div>
    </>
  )
}
