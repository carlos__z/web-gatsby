import React, { useState } from 'react'
import Helmet from 'react-helmet'
import { ThemeProvider } from 'emotion-theming'
import GlobalStyles from '../styles/global'
import { lightTheme, darkTheme } from '../styles/theme'
import Header from './header'
import Footer from './footer'

const Layout = (props) => {
  const [ theme, setTheme ] = useState('dark');
  
  const toggleTheme = () => {
    if (theme === 'dark') {
      setTheme('light');
    } else {
      setTheme('dark');
    }
  };
  
  return (  
    <>
      <ThemeProvider theme={theme === 'dark' ? darkTheme : lightTheme} >
        <GlobalStyles />
        <Helmet>
          <html lang="es" />
          <title>Carlos Amorós - Desarrollador Web</title>
          <link rel="stylesheet" href="https://unpkg.com/flickity@2/dist/flickity.min.css" />
        </Helmet>
        <Header />
        {/* <button
          onClick={toggleTheme}
        >TEMA</button> */}
        {props.children}     
        <Footer />
      </ThemeProvider>
    </>
  );
}

export default Layout;
