import React from 'react';
import { Link } from 'gatsby'
import Logo from './ui/logo'
import WorksIcon from './ui/worksIcon'
import MeIcon from './ui/meIcon'
import ContactIcon from './ui/contactIcon'
import styled from '@emotion/styled'

const themeColor = `#ffffff`;

const NavIcons = styled.nav`
  margin: 0 auto;
  display: flex;
  align-items: center;
  justify-content: center;
  flex-direction: column;
  position: fixed;
  top: 35%;
  right: 1.5rem;
  z-index: 999;
`;

const NavLink = styled(Link)`
  line-height: 5.8rem;
  text-decoration: none;
  svg {
    fill: #fafafa;
    /* .active {
      fill: #DDA448!important;
    } */
    width: 3.6rem;
    @media (max-width: 768px) {
      width: 3rem;
    }
  }
`;

const Nav = () => {
  return (  
    <NavIcons>
      {/* TODO: Cambio ícono principal y ver de animarlo */}
      <NavLink to="/" activeClassName="active"><Logo fill={themeColor} /></NavLink>
      <NavLink to="/works" activeClassName="active"><WorksIcon /></NavLink>
      <NavLink to="/me" activeStyle={{ fill: "#DDA448" }}><MeIcon /></NavLink>
      <NavLink to="/contact" activeClassName="active"><ContactIcon fill={themeColor} /></NavLink>
    </NavIcons>
  );
}

export default Nav;
