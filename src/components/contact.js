import React from 'react'
import { graphql, useStaticQuery } from 'gatsby'
import BackgroundImage from 'gatsby-background-image'
import styled from '@emotion/styled'

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  p {
    font-size: 2.4rem;
  }

  @media (max-width: 768px) {
    flex-direction: column; 
    p {
      font-size: 2rem;
    }
  }
`;

const Column = styled.div`
  width: 50%;
  flex-direction: column;
  justify-content: center;
  text-align: center;
  @media (max-width: 768px) {
    width: 100%;
  }
`;

const ContactBg = styled(BackgroundImage)`
  height: 100vh;
  background-position: left top;
  background-repeat: no-repeat;
  @media (max-width: 768px) {
    height: 80vh;
  }
`;

const Button = styled.a`
  padding: 1rem;
  background-color: #fafafa;
  color: #121212;
  transition: background-color 0.5s, color 0.5s, transform 0.5s;
  border-radius: 0.2rem;
  /* TODO: separación footer y botón */
  :hover {
    color: #fafafa;
    background-color: #121212;
  }
`;

export default function Contact() {
  const { image } = useStaticQuery(graphql`
    query {
      image: file(relativePath: {eq: "contact-bg.jpg"}) {
        sharp: childImageSharp {
          fluid(quality: 90) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
    }
  `);
  
  return (
    <>
      <Container>
        <Column>
          <ContactBg 
            tag="section"
            fluid={image.sharp.fluid}
            fadeIn="soft"
          />
        </Column>
        <Column>
          <p>me@carlosamoros.com</p>
          <Button href="https://calendly.com/carlos_" target="_blank" rel="noopener noreferrer">Conversemos un rato</Button>
        </Column>
      </Container>
    </>
  )
}
