import React from 'react'
import { graphql, useStaticQuery } from 'gatsby'
import BackgroundImage from 'gatsby-background-image'
import styled from '@emotion/styled'

const HeroBg = styled(BackgroundImage)`
  height: 100vh;
  background-position: cover;
  background-repeat: no-repeat;

  .overlay {
    background: linear-gradient(90deg, rgba(20,20,20,0.1) 80%, rgba(20,20,20,0.85) 95%);
    height: 100%;
    @media (max-width: 768px) {
      background: linear-gradient(90deg, rgba(20,20,20,0.1) 85%, rgba(20,20,20,0.5) 95%);
    }
  }
`;

const Container = styled.div`
  display: flex;
  align-items: center;
  justify-content: space-between;
  width: 100%;
  p {
    font-size: 4.2rem;
  }
  @media(min-width: 768px) {
    .main-text {
      padding-left: 5.1rem;
    }
  }
  @media (max-width: 768px) {
    flex-direction: column;
    p {
      font-size: 2.4rem;
    }
    .main-text {
      padding: 2.4rem 1rem;
    }
  }
`;

const Column = styled.div`
  width: 50%;
  @media (max-width: 768px) {
    width: 100%;
    text-align: center;
  }
`;

export default function Hero() {
  const { image } = useStaticQuery(graphql`
    query {
      image: file(relativePath: {eq: "hero-bg.jpg"}) {
        sharp: childImageSharp {
          fluid(quality: 90) {
            ...GatsbyImageSharpFluid_withWebp
          }
        }
      }
    }
  `);
  
  return (
    <>
      <Container>
        <Column
          className="main-text"
        >
          <h1>Carlos Amorós</h1>
          <p>Desarrollador web</p>
        </Column>
        <Column>
          <HeroBg 
            tag="section"
            fluid={image.sharp.fluid}
            fadeIn="soft"
          >
            <div className="overlay"></div>
          </HeroBg>
        </Column>
      </Container>
    </>
  )
}
