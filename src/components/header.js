import React from 'react';
import { css } from '@emotion/core'
import Nav from './nav'

const Header = () => {
  return (  
    <header
      css={css`
        display: flex;
        align-items: center;
        justify-content: center;
        flex-direction: column;
      `}
    >
      <Nav />
    </header>
  );
}

export default Header;
