import React from 'react'
import { FaBitbucket, FaGithub, FaLinkedinIn, FaBehance } from 'react-icons/fa'
import styled from '@emotion/styled'

const ContainerFooter = styled.footer`
  margin: 0 auto;
  position: fixed;
  bottom: 0;
  right: 5.1rem;
  width: 100%;
  @media (max-width: 768px) {
    position: relative;
    padding: 1.6rem 1rem;
    right: 0;
  }
  a {
    color: #fff;
    font-size: 3.2rem;
    padding-left: 1.6rem;
  }
  nav {
    text-align: right;
    @media (max-width: 768px) {
      text-align: center;
      svg {
        font-size: 2.4rem;
      }
    }
  }
`;

const Footer = () => {
  return (  
    <ContainerFooter>
      <nav>
        <a
          href="https://bitbucket.org/carlos__z/"
          target="_blank"
          title="Mira mi trabajo en Bitbucket"
          rel="noopener noreferrer"
          aria-label="Bitbucket"
        ><FaBitbucket /></a>
        <a
          href="https://github.com/carlosz44"
          target="_blank"
          title="Mira mi trabajo en GitHub"
          rel="noopener noreferrer"
          aria-label="GitHub"
        ><FaGithub /></a>
        <a
          href="https://www.linkedin.com/in/carlos-amoros/"
          target="_blank"
          title="Contacta a Carlos a través de LinkedIn"
          rel="noopener noreferrer"
          aria-label="LinkedIn"
        ><FaLinkedinIn /></a>
        <a
          href="https://www.behance.net/carlos__z"
          target="_blank"
          title="Mira el portafolio de Carlos en Behance"
          rel="noopener noreferrer"
          aria-label="Behance"
        ><FaBehance /></a>
      </nav>
    </ContainerFooter>
  );
}

export default Footer;
