import React from 'react'
import { graphql, useStaticQuery } from 'gatsby'
import styled from '@emotion/styled'
import { css } from '@emotion/core'
import Flickity from 'react-flickity-component'
import 'flickity/css/flickity.css'

const SliderContainer = styled(Flickity)`
  margin: 0 auto;
  padding: 1rem 3.6rem;
  overflow: hidden;

  .flickity-prev-next-button.previous {
    left: 4rem;
  }
  .flickity-prev-next-button.next {
    right: 4rem;
  }
  .carousel.is-fullscreen .carousel-cell {
    height: 100%;
  }
`;

const Slide = styled.div`
  display: flex;
  @media (max-width: 768px) {
    flex-direction: column;
  }
  width: 66%;
  margin: 0 2rem;
  border-radius: 0.5rem;
  @media (min-width: 768px) {
    height: 50rem;
    width: 80%;
  }
  @media (max-width: 768px) {
    width: 80%;
    padding-top: 2rem;
  }
  .overlay {
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    height: 100%;
    width: 100%;
    opacity: 0;
    transition: .5s ease;
    display: flex;
    align-items: center;
    justify-content: center;
    flex-direction: column;
  }
  :hover .overlay {
    opacity: 1;
  }
`;

const ImgContainer = styled.div`
  width: 50%;
  display: flex;
  align-items: center;
  justify-content: center;
  @media (max-width: 768px) {
    width: 100%;
  }
  img {
    width: 40rem;
    @media (max-width: 768px) {
      width: 100%;
    }
  }
`;

export default function WorkSlider() {
  const result = useStaticQuery(graphql`
    {
      allDatoCmsWork {
        nodes {
          title
          description
          imgdesktop {
            fluid {
              ...GatsbyDatoCmsFluid
            }
          }
          imgmobile {
            fluid {
              ...GatsbyDatoCmsFluid
            }
          }
          hexcolor
          url
        }
      }
    }
  `);
  
  const flickityOptions = {
    initialIndex: 2,
    freeScroll: false,
    wrapAround: true,
    lazyLoad: true,
    selectedAttraction: 0.02,
    friction: 0.1,
  };

  return (
    <>
      <SliderContainer
        className={'carousel'} 
        elementType={'div'}
        options={flickityOptions}
      >
        {result.allDatoCmsWork.nodes.map((work, index) => (
          <Slide 
            key={index}
            css={css`
              background-color: ${work.hexcolor};
              @media (min-width: 768px) {
                background-image: url(${work.imgdesktop.fluid.src});
                background-repeat: no-repeat;
                background-position: center;
                background-size: 80%;
              }
            `}
          >
            <ImgContainer
              css={css`
                @media (min-width: 768px) {
                  padding-top: 3rem;
                }
              `}
            >
              <img 
                src={work.imgmobile.fluid.srcSet} 
                alt={work.title}
              />
            </ImgContainer>
            <ImgContainer 
              css={css`
                @media (min-width: 768px) {
                  display: none;
                }
              `}>
              <img src={work.imgdesktop.fluid.srcSet} alt={work.title} />
            </ImgContainer>
            <div 
              className="overlay"
              css={css`
                background-color: ${work.hexcolor};
              `}
            >     
              <a href={work.url} target="_blank" rel="noopener noreferrer"><h3>{work.title}</h3></a>
            </div>
          </Slide>
        ))}
      </SliderContainer>
    </>
  )
}
